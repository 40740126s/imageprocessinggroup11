import os
import cv2
import numpy as np
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from sklearn.datasets import make_blobs
#---------------------------------#
folder_path_train = r"train"
folder_path_test = r"test"

#---------------------------------#
#---------------------------------#
#---------------------------------#


def binarize_image(image_path):
    # 讀取影像
    image = cv2.imread(image_path, 0)  # 以灰階模式讀取影像
    # 使用Otsu方法找到最佳門檻值
    _, binary_image = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return binary_image

#---------------------------------------------------------------------#

def process_images_in_folder(folder_path):
    runLengthResult = []

    # 處理資料夾內的影像
    for filename in os.listdir(folder_path):
        if filename.endswith(".jpg") or filename.endswith(".png"):
            RL = [0,0,0,0,0,0,0,0]  # [b0,b45,b90,b135,w0,w45,w90,w135]

            image_path = os.path.join(folder_path, filename)
            # 進行影像二值化處理
            image = binarize_image(image_path)
            #print(f"{filename} run-length...")
            height, width = image.shape
            
            ## 00000
            for x in range(1,height):
                runs=1
                for y in range(1,width):
                    #print(image[x][y])
                    if(image[x][y-1] == image[x][y]):
                        runs += 1
                    else:
                        runs *= runs
                        if(image[x][y]==0):
                            RL[0] = RL[0]+runs
                        else:
                            RL[4] = RL[4]+runs
                        runs = 1
                        
                    if(y==width-1):
                        runs *= runs
                        if(image[x][y]==0):
                            RL[0] = RL[0]+runs
                        else:
                            RL[4] = RL[4]+runs     
            #print("0",RL)
            ## 00000

            ## 454545
            for x in range(1,height):
                runs=1
                steps = x  # max = height-1
                try:
                    for y in range(1,steps):
                        if(image[x-y][y] == image[x-y-1][y+1]):
                            runs += 1
                        else:
                            runs *= runs
                            if(image[x-y][y]==0):
                                RL[1] = RL[1]+runs
                            else:
                                RL[5] = RL[5]+runs
                            runs = 1
                        
                        if(y==steps-1):
                            runs *= runs
                            if(image[x-y][y]==0):
                                RL[1] = RL[5]+runs
                            else:
                                RL[1] = RL[5]+runs
                except:
                    pass
            for y in range(2,width-2):
                runs = 1
                steps = height-y
                try:
                    for x in range(1,steps):
                        if(image[height-x][y+x-1] == image[height-x-1][y+x]):
                            runs = runs + 1
                        else:
                            runs = runs*runs
                            if(image[height-x-1][y+x]==0):
                                RL[1] = RL[1]+runs
                            else:
                                RL[5] = RL[5]+runs
                            runs = 1

                        if(x==steps-1):
                            runs = runs*runs
                            if(image[height-x-1][y+x]==0):
                                RL[1] = RL[1]+runs
                            else:
                                RL[5] = RL[5]+runs
                            runs = 1
                except:
                    pass
            #print("45",RL)
            ## 454545

            ## 909090
            for y in range(1,width):
                runs = 1
                for x in range(1,height):
                    if(image[height-x-1][y] == image[height-x-2][y]):
                        runs = runs + 1
                    else:
                        runs = runs*runs
                        if(image[height-x-2][y]==0):
                            RL[2] = RL[2]+runs
                        else:
                            RL[6] = RL[6]+runs
                        runs = 1

                    if(x==height-2):
                        runs = runs*runs
                        if(image[height-x-2][y]==0):
                            RL[2] = RL[2]+runs
                        else:
                            RL[6] = RL[6]+runs
            #print("90",RL)
            ## 909090

            ## 135135
            for x in range(1,width):
                runs = 1
                steps = x
                w = width
                for y in range(1,steps):
                    w = w-1
                    try:
                        if(image[x-y][w] == image[x-y][w-1]):
                            runs = runs+1
                        else:
                            runs = runs*runs
                            if(image[x-y][w]==0):
                                RL[3] = RL[3]+runs
                            else:
                                RL[7] = RL[7]+runs
                            runs = 1

                        if (y==steps-1):
                            runs = runs*runs
                            if(image[x-y][w]==0):
                                RL[3] = RL[3]+runs
                            else:
                                RL[7] = RL[7]+runs    
                    except:
                        pass
            for y in range(1,width-1):
                runs = 1
                steps = height - y
                w = width - y
                for x in range(1,steps):
                    try:
                        w = w-1
                        if(image[height-x][w] == image[height-x][w-1]):
                            runs = runs+1
                        else:
                            runs = runs*runs
                            if(image[height-x][w]==0):
                                RL[3] = RL[3]+runs
                            else:
                                RL[7] = RL[7]+runs
                            runs = 1   

                        if (x == steps-1):
                            runs = runs*runs
                            if(image[height-x][w]==0):
                                RL[3] = RL[3]+runs
                            else:
                                RL[7] = RL[7]+runs
                    except:
                        pass
            #print("135",RL)
            ## 135135

            runLengthResult.append(RL)

    print(runLengthResult)
    return runLengthResult


def run_Kmeans(X,Y):

    # 初始化 K-means 分群器
    kmeans = KMeans(n_clusters=5)

    # 將資料集 X 進行分群
    kmeans.fit(X)
    #y_kmeans = kmeans.predict(X)
    #plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')

    # 獲取分群結果
    labels = kmeans.labels_
    print(labels)

    # 獲取分群中心點
    centroids = kmeans.cluster_centers_
    print(centroids)

    clusters_pred = kmeans.predict(Y)
    print(clusters_pred)
    clusters_pred2 = kmeans.fit_predict(X)
    print(clusters_pred2)
  

print("================================")
print("處理開始...")
X = process_images_in_folder(folder_path_train)
Y = process_images_in_folder(folder_path_test)
run_Kmeans(X,Y)
print("處理結束...")
print("================================")

